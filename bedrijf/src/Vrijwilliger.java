/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Vrijwilliger extends Persoon implements Oproepbaar {

    public Vrijwilliger(String naam) {
        super(naam);
    }

    @Override
    public double berekenInkomsten() {
        return 0.00;
    }

    @Override
    public void huurIn(int uren) {

    }
}
