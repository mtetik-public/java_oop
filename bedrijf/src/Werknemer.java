/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Werknemer extends Persoon {

    private static int laatstePersoneelsnummer = 0;

    private double maandSalaris;
    private int personeelsnummer;

    public Werknemer(double maandSalaris, String naam) {
        super(naam);
        this.maandSalaris = maandSalaris;
        this.personeelsnummer = ++ laatstePersoneelsnummer;
    }

    @Override
    public double berekenInkomsten() {
        return maandSalaris;
    }
}
