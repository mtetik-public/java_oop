/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Zzper extends Persoon implements Oproepbaar {

    private double uurtarief;
    private int urenGewerkt;

    public Zzper(double uurtarief, String naam) {
        super(naam);
        this.uurtarief = uurtarief;
        this.urenGewerkt = 0;
    }

    public double berekenInkomsten() {
        return uurtarief * urenGewerkt;
    }

    public void huurIn(int uren) {
        urenGewerkt += uren;
    }

}
