/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public abstract class Persoon implements Comparable<Persoon> {

    private String naam;

    public Persoon(String naam) {
        this.naam = naam;
    }

    public abstract double berekenInkomsten();

    public int compareTo(Persoon other) {
        return naam.compareTo(other.naam);
    }

    public String toString() {
        StringBuilder content = new StringBuilder(String.format(
                "%s",
                naam
        ));

        return content.toString();
    }
}
