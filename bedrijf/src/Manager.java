/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Manager extends Werknemer {

    private double bonus;

    public Manager(double maandSalaris, String naam) {
        super(maandSalaris, naam);
        this.bonus = 0.00;
    }

    public void kenBonusToe(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public double berekenInkomsten() {
        return super.berekenInkomsten() + this.bonus;
    }
}
