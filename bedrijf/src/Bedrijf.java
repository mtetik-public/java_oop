import java.util.ArrayList;

/**
 * bedrijf - 10/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Bedrijf {

    private String naam;
    private ArrayList<Persoon> medewerkers;

    public Bedrijf(String naam) {
        this.naam = naam;
        this.medewerkers = new ArrayList<Persoon>();
    }

    public void printInkomsten() {
        System.out.println("Inkomsten van alle personen:");

        for (int i = 0; i < medewerkers.size(); i++) {
            System.out.print("\t" + medewerkers.get(i) + "\t");

            double inkomst = medewerkers.get(i).berekenInkomsten();

            if (inkomst != 0) {
                System.out.print("Inkomst: " + inkomst + "\n");
            } else {
                System.out.print("Bedankt voor uw inzet!\n");
            }
        }
    }

    public int aantalManagers() {
        int aantal = 0;

        for (int i = 0; i < medewerkers.size(); i++) {
            if (medewerkers.get(i) instanceof Manager) {
                aantal++;
            }
        }

        return aantal;
    }

    public void neemInDienst(Persoon persoon) {
        medewerkers.add(persoon);
    }

    public String toString() {
        String text = "Bedrijf " + this.naam + " heeft " + medewerkers.size() + " in dienst\n";

        for (Persoon p : medewerkers) {
            text += "\t" + p + " \n";
        }

        return text;
    }
}
