/**
 * bedrijf - 09/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public interface Oproepbaar {

    public void huurIn(int uren);
}
