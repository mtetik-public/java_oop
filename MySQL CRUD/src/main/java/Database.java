import java.sql.*;

/**
 * @author Mehmet TETIK
 *
 * Creating/making a database connection
 *
 * C: insert query
 * R: select query
 * U: update query
 * D: delete query
 */
public class Database {

    // Database hostname
    // if localhost => "jdbc:mysql://localhost/databaseName?useLegacyDatetimeCode=false&serverTimezone=UTC
    private static final String DB_HOST = "jdbc:mysql://localhost/test?useLegacyDatetimeCode=false&serverTimezone=UTC";
    // Database username
    private static final String DB_USERNAME = "root";
    // Database password
    private static final String DB_PASSWORD = "";
    // Database driver, in this case: JDBC
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    // Show executed time messages
    private static final boolean SHOW_TIME_ELAPSED = true;

    // Define the connection
    private static Connection databaseConnection;


    /**
     * Create database connection
     *
     * @return
     * @throws Exception
     */
    private static Connection dbConnect() throws Exception {

        try {
            // Set driver class
            Class.forName(DB_DRIVER);

            // Prepare the connection
            databaseConnection = DriverManager.getConnection(DB_HOST, DB_USERNAME, DB_PASSWORD);

            /**
             * Connection established...
             *
             * System.out.println("Database connection established\n");
             */

            return databaseConnection;
        } catch (SQLException ex) {
            errorHandling(ex);
        }

        return null;
    }

    /**
     * Select query
     *
     * @param  query        SQL select query
     * @throws Exception
     */
    public static void selectQuery(String query) throws Exception {

        // Establish database connection
        dbConnect();

        // Set start timer
        double start = System.nanoTime();

        Statement statement = databaseConnection.createStatement();
        ResultSet result = statement.executeQuery(query);

        double end = System.nanoTime();

        // Count all columns of a table
        int getColumns = result.getMetaData().getColumnCount();

        System.out.printf("%-10s %s%n%n", "Query (SELECT):", query);

        try {
            // Print out all data from table
            while (result.next()) {
                for (int i = 1; i <= getColumns; i++) {
                    String tableRowData = result.getString(i);
                    String tableColumnName = result.getMetaData().getColumnName(i);
                    String tableColumnType = result.getMetaData().getColumnTypeName(i);

                    System.out.printf("[%-15s (%s: %s)]  ", tableRowData, tableColumnName, tableColumnType);
                }

                System.out.println();
            }

            if(SHOW_TIME_ELAPSED) {
                System.out.println(getExecutionTime(end, start));
            }
        } catch (SQLException ex) {
            errorHandling(ex);
        } finally {
            // Close connection
            result.close();
            statement.close();
            databaseConnection.close();
        }
    }

    /**
     * Insert query
     *
     * @param  query        SQL insert query
     * @param  values       Values to be inserted
     * @throws Exception
     */
    public static void insertQuery(String query, Object... values) throws Exception {

        // Establish database connection
        dbConnect();

        // Set start timer
        double start = System.nanoTime();

        // Foreach value given, check & format the value
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof String) {
                values[i] = "'" + values[i] + "'";
            }
        }

        // Format the query
        String interpolatedQuery = String.format(query, values);

        // Query (prepare statement)
        PreparedStatement preparedStatement = databaseConnection.prepareStatement(interpolatedQuery);

        System.out.printf("%-10s %s%n%n", "Query (INSERT):", interpolatedQuery);

        try {
            // Insert data
            preparedStatement.execute();
            double end = System.nanoTime();

            System.out.println("-- record affected --");

            if(SHOW_TIME_ELAPSED) {
                System.out.println(getExecutionTime(end, start));
            }
        } catch (SQLException ex) {
            errorHandling(ex);
        } finally {
            // Close connection
            preparedStatement.close();
            databaseConnection.close();
        }
    }

    /**
     * Update query
     *
     * @param  query        SQL update query
     * @param  values       Values to be updated
     * @throws Exception
     */
    public static void updateQuery(String query, Object... values) throws Exception {

        // Establish database connection
        dbConnect();

        // Set start timer
        double start = System.nanoTime();

        // Foreach value given, check & format the value
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof String) {
                values[i] = "'" + values[i] + "'";
            }
        }

        // Format the query
        String interpolatedQuery = String.format(query, values);

        // Query (prepare statement)
        PreparedStatement preparedStatement = databaseConnection.prepareStatement(interpolatedQuery);

        System.out.printf("%-10s %s%n%n", "Query (UPDATE):", interpolatedQuery);

        try {
            int rowsUpdated = preparedStatement.executeUpdate();
            double end = System.nanoTime();

            // If row exists and is updated
            if(rowsUpdated > 0) {
                System.out.println("-- record updated --");
            }else{
                System.out.println("Record doesn't exists");
            }

            if(SHOW_TIME_ELAPSED) {
                System.out.println(getExecutionTime(end, start));
            }
        } catch (SQLException ex) {
            errorHandling(ex);
        } finally {
            // Close connection
            preparedStatement.close();
            databaseConnection.close();
        }
    }

    /**
     * Delete query
     *
     * @param  query        SQL delete query
     * @param  values       Values to be deleted
     * @throws Exception
     */
    public static void deleteQuery(String query, Object... values) throws Exception {

        // Establish database connection
        dbConnect();

        // Set start timer
        double start = System.nanoTime();

        // Foreach value given, check & format the value
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof String) {
                values[i] = "'" + values[i] + "'";
            }
        }

        // Format the query
        String interpolatedQuery = String.format(query, values);

        // Query (prepare statement)
        PreparedStatement preparedStatement = databaseConnection.prepareStatement(interpolatedQuery);

        System.out.printf("%-10s %s%n%n", "Query (DELETE):", interpolatedQuery);

        try {
            int rowsDeleted = preparedStatement.executeUpdate();
            double end = System.nanoTime();

            // If row exists and is deleted
            if(rowsDeleted > 0) {
                System.out.println("-- record deleted --");
            }else{
                System.out.println("Record doesn't exists");
            }

            if(SHOW_TIME_ELAPSED) {
                System.out.println(getExecutionTime(end, start));
            }
        } catch (SQLException ex) {
            errorHandling(ex);
        } finally {
            // Close connection
            preparedStatement.close();
            databaseConnection.close();
        }
    }

    /**
     * Error handling MySQL
     *
     * @param  exception        SQLException
     * @throws Exception
     */
    private static void errorHandling(SQLException exception) throws Exception {
        System.out.println("---- Error ----");
        System.out.printf("%-17s%s%n", "SQLException:", exception.getMessage());
        System.out.printf("%-17s%s%n", "SQLState:", exception.getSQLState());
        System.out.printf("%-17s%s%n", "VendorError:", exception.getErrorCode());
        System.out.println("---------------");
    }

    /**
     * Get time difference in milliseconds
     *
     * @param endTime       End time
     * @param startTime     Start time
     * @return
     */
    private static String getExecutionTime(double endTime, double startTime)
    {
        double timeDifference = (endTime - startTime) / 1e6;

        return "Query took " + String.format("%.2f", timeDifference) + " ms";
    }
}
