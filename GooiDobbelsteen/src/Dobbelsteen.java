/**
 * GooiDobbelsteen - 12/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 */
import java.util.Random;

public class Dobbelsteen {
    Random getalGenerator = new Random();

    int worp;

    void gooi() {
        worp = getalGenerator.nextInt(6) + 1;
    }

    void print() {
        switch (worp){
            case(1):
                System.out.println(" " + worp);
                break;
            case(2):
                System.out.println(worp);
                System.out.println(" " + worp);
                break;
            case(3):
                System.out.println(worp);
                System.out.println(" " + worp);
                System.out.println(" " + " " + worp);
                break;
            case(4):
                System.out.println(worp + " " + worp);
                System.out.println("");
                System.out.println(worp + " " + worp);
                break;
            case(5):
                System.out.println(worp + " " + worp);
                System.out.println(" " + worp);
                System.out.println(worp + " " + worp);
                break;
            case(6):
                System.out.println(worp + " " + worp);
                System.out.println(worp + " " + worp);
                System.out.println(worp + " " + worp);
                break;
        }
    }

    int getWorp() {
        return worp;
    }
}
