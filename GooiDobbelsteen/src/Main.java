/**
 * GooiDobbelsteen - 12/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 * Gooien met dobbelsteen tot dat er '6' wordt gegooid.
 */
public class Main {
    public static void main(String[] args) {
        Dobbelsteen steen = new Dobbelsteen();

        while(steen.getWorp() != 6){
            steen.gooi();

            steen.print();
            System.out.println();
        }
    }
}
