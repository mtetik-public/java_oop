package models;

/**
 * HappyNewYear startproject - 07/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Knaller extends Vuurwerk {

    private final static int MAX_DECIBEL = 120;
    private int aantalKnallen;
    private int decibel;

    public Knaller(String naam, double prijs, int aantalKnallen, int decibel, Instructie instructie) {
        super(naam, prijs, instructie);
        this.aantalKnallen = aantalKnallen;
        this.decibel = decibel;
    }

    public int getDecibel() {
        return decibel;
    }

    @Override
    public boolean isLegaal() {
        return super.isLegaal() && decibel <= MAX_DECIBEL;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nAantal knallen: " + aantalKnallen +
                "\nDecibel: " + decibel;
    }
}
