package models;

/**
 * Purpose: Bijhouden van instructies
 */

public class Instructie {

    private boolean nederlandstalig;
    private int minimumLeeftijd;
    private String omschrijving;

    /**
     * Maak instructie
     *
     * @param nederlandstalig boolean nederlandstalig
     * @param minimumLeeftijd int minimum leeftijd
     * @param omschrijving string omschrijving
     */
    public Instructie(boolean nederlandstalig, int minimumLeeftijd, String omschrijving) {
        this.nederlandstalig = nederlandstalig;
        this.minimumLeeftijd = minimumLeeftijd;
        this.omschrijving = omschrijving;
    }

    /**
     *
     * @return boolean nederlandstalig
     */
    public boolean isNederlandstalig() {
        return nederlandstalig;
    }

    /**
     *
     * @return int verkrijg leeftijd
     */
    public int getLeeftijd() {
        return minimumLeeftijd;
    }

    /**
     *
     * @return string productinformatie
     */
    @Override
    public String toString() {
        return "Nederlandstalig=" + nederlandstalig + ", leeftijd=" + minimumLeeftijd + ", omschrijving=" + omschrijving;
    }
}


