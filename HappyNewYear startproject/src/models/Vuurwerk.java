package models;

/**
 * HappyNewYear startproject - 07/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Vuurwerk extends Product {

    private Instructie instructie;

    public Vuurwerk(String naam, double prijs, Instructie instructie) {
        this(naam, prijs);
        this.instructie = instructie;
    }

    public Vuurwerk(String naam, double prijs) {
        super(naam, prijs);
    }

    public Instructie getInstructie() {
        return instructie;
    }

    @Override
    public boolean isLegaal() {
        if(instructie != null && instructie.isNederlandstalig()) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nInstructie: " + ((instructie != null) ? instructie.toString() : "ontbreekt") +
                "\nLegaal: " + isLegaal();
    }
}
