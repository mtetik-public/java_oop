package models;

/**
 * HappyNewYear startproject - 07/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Vuurpijl extends Vuurwerk {

    private final static int MIN_LEEFTIJD = 16;
    private final static int MAX_WAARDE = 100;
    private final static int MIN_WAARDE = 0;

    private double hoogte; // in meters

    private int[] kleurverhouding;
    private String[] kleuren = {"ROOD", "GROEN", "BLAUW"};

    public Vuurpijl(String naam, double prijs, double hoogte, int[] kleurverhouding, Instructie instructie) {
        super(naam, prijs, instructie);
        this.hoogte = hoogte;

        if(correcteKleurverhouding(kleurverhouding)) {
            this.kleurverhouding = kleurverhouding;
        } else {
            System.out.println("--> FOUT: Onjuiste kleurverhouding, kleur wordt ROOD!");
            this.kleurverhouding = new int[] {MAX_WAARDE, MIN_WAARDE, MIN_WAARDE};
        }
    }

    private boolean correcteKleurverhouding(int[] kv) {
        return (kv[0] + kv[1] + kv[2]) == MAX_WAARDE;
    }

    @Override
    public boolean isLegaal() {
        return super.isLegaal() && super.getInstructie().getLeeftijd() >= MIN_LEEFTIJD;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nHoogte: " + hoogte + " meter" +
                "\nKleuren: " +
                    "\n" + kleuren[0] + ": " + kleurverhouding[0] + "%" +
                    "\n" + kleuren[1] + ": " + kleurverhouding[1] + "%" +
                    "\n" + kleuren[2] + ": " + kleurverhouding[2] + "%";
    }
}
