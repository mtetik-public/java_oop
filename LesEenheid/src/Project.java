/**
 * LesEenheid - 09/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Nieuwe project/vak kunnen aanmaken
 */
public class Project extends LesEenheid {
    private double productCijfer;
    private double procesCijfer;
    private double methodenEnTechniekCijfer;

    /**
     *
     * @param productCijfer cijfer behaald voor product
     * @param procesCijfer cijfer behaald voor proces
     * @param methodenEnTechniekCijfer cijfer behaald voor methoden & techniek
     * @param naam naam van vak
     * @param ects aantal studiepunten dat te behalen is voor vak
     * @param studiejaar studiejaar van het vak
     */
    public Project(
            double productCijfer, double procesCijfer,
            double methodenEnTechniekCijfer, String naam,
            int ects, int studiejaar
    ) {
        super(naam, ects, studiejaar);
        this.productCijfer = productCijfer;
        this.procesCijfer = procesCijfer;
        this.methodenEnTechniekCijfer = methodenEnTechniekCijfer;
    }

    /**
     *
     * @param naam naam van vak
     * @param ects aantal studiepunten dat te behalen is voor vak
     * @param studiejaar studiejaar van het vak
     */
    public Project(String naam, int ects, int studiejaar) {
        this(DEFAULT_CIJFER, DEFAULT_CIJFER, DEFAULT_CIJFER, naam, ects, studiejaar);
    }

    /**
     *
     * @param productCijfer nieuw cijfer voor product
     */
    public void setProductcijfer(double productCijfer) {
        this.productCijfer = productCijfer;
    }

    /**
     *
     * @param procesCijfer nieuw cijfer voor proces
     */
    public void setProcescijfer(double procesCijfer) {
        this.procesCijfer = procesCijfer;
    }

    /**
     *
     * @param methodenEnTechniekCijfer nieuw cijfer voor methoden & techniek
     */
    public void setMethodenEnTechniekenCijfer(double methodenEnTechniekCijfer) {
        this.methodenEnTechniekCijfer = methodenEnTechniekCijfer;
    }

    /**
     *
     * @return alle data voor vak
     */
    @Override
    public String toString() {
        StringBuilder content = new StringBuilder(String.format(
                "%s (%.6f, %.6f, %.6f)",
                super.toString(),
                productCijfer,
                procesCijfer,
                methodenEnTechniekCijfer
        ));

        return content.toString();
    }

    /**
     *
     * @return true of false als het vak is afgerond
     */
    public String isAfgerond() {
        // zet default waarde
        String isAfgerond = "Nee";

        // als product, proces, methoden&techniek cijfer groter/gelijk is aan 5.5
        if(
            productCijfer >= ONDERGRENS_VOLDOENDE &&
            procesCijfer >= ONDERGRENS_VOLDOENDE &&
            methodenEnTechniekCijfer >= ONDERGRENS_VOLDOENDE
        ) {
            isAfgerond = "Ja";
        }

        return isAfgerond;
    }
}
