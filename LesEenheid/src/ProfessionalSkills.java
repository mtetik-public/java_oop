/**
 * LesEenheid - 09/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Nieuwe professional skill/vak kunnen aanmaken
 */
public class ProfessionalSkills extends LesEenheid {
    private boolean gehaald;

    /**
     *
     * @param gehaald geef aan of het vak is gehaald
     * @param naam naam van het vak
     * @param ects aantal studiepunten dat te behalen is voor het vak
     * @param studiejaar studiejaar van het vak
     */
    public ProfessionalSkills(boolean gehaald, String naam, int ects, int studiejaar) {
        super(naam, ects, studiejaar);
        this.gehaald = gehaald;
    }

    /**
     *
     * @param naam naam van het vak
     * @param ects aantal studiepunten dat te behalen is voor het vak
     * @param studiejaar studiejaar van het vak
     */
    public ProfessionalSkills(String naam, int ects, int studiejaar) {
        this(DEFAULT_GEHAALD, naam, ects, studiejaar);
    }

    /**
     *
     * @param gehaald geef aan of het vak is gehaald
     */
    public void setGehaald(boolean gehaald) {
        this.gehaald = gehaald;
    }

    /**
     *
     * @return alle data voor vak
     */
    @Override
    public String toString() {
        String waardeGehaald = "niet";

        if(gehaald) {
            waardeGehaald = "wel";
        }

        StringBuilder content = new StringBuilder(String.format(
                "%s, %s gehaald",
                super.toString(),
                waardeGehaald
        ));

        return content.toString();
    }

    /**
     *
     * @return true of false als het vak is afgerond
     */
    public String isAfgerond() {
        // default waarde afgerond
        String isAfgerond = "Nee";

        // als het vak gehaald is
        if(gehaald) {
            isAfgerond = "Ja";
        }

        return isAfgerond;
    }

}
