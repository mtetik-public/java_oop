/**
 * LesEenheid - 08/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Nieuwe vak kunnen aanmaken
 */
public class Vak extends LesEenheid {
    private double cijfer;

    /**
     *
     * @param cijfer cijfer dat behaald is voor het vak
     * @param naam naam van het vak
     * @param ects aantal studiepunten dat te behalen is voor het vak
     * @param studiejaar studiejaar van het vak
     */
    public Vak(double cijfer, String naam, int ects, int studiejaar) {
        super(naam, ects, studiejaar);
        this.cijfer = cijfer;
    }

    /**
     *
     * @param naam naam van het vak
     * @param ects aantal studiepunten dat te behalen is voor het vak
     * @param studiejaar studiejaar van het vak
     */
    public Vak(String naam, int ects, int studiejaar) {
        this(DEFAULT_CIJFER, naam, ects, studiejaar);
    }

    /**
     *
     * @param cijfer nieuwe cijfer voor vak
     */
    public void setCijfer(double cijfer) {
        this.cijfer = cijfer;
    }

    /**
     *
     * @return alle data van vak
     */
    @Override
    public String toString() {
        StringBuilder content = new StringBuilder(String.format(
            "%s, cijfer %.1f",
            super.toString(),
            cijfer
        ));

        return content.toString();
    }

    /**
     *
     * @return true of false als het vak is afgerond
     */
    public String isAfgerond() {
        String isAfgerond = "Nee";

        // als cijfer groter/gelijk is aan 5.5
        if(cijfer >= ONDERGRENS_VOLDOENDE) {
            isAfgerond = "Ja";
        }

        return isAfgerond;
    }

}
