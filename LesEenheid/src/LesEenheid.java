/**
 * LesEenheid - 08/12/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Nieuwe vak kunnen aanmaken
 */
public class LesEenheid {
    protected final static double DEFAULT_CIJFER = -1;
    protected final static boolean DEFAULT_GEHAALD = false;
    protected final static double ONDERGRENS_VOLDOENDE = 5.5;

    private String naam;
    private int ects;
    private int studiejaar;

    /**
     *
     * @param naam naam van het vak
     * @param ects aantal studiepunten dat te behalen is voor het vak
     * @param studiejaar studiejaar van het vak
     */
    public LesEenheid(String naam, int ects, int studiejaar) {
        this.naam = naam;
        this.ects = ects;
        this.studiejaar = studiejaar;
    }

    /**
     *
     * @return alle data van vak
     */
    public String toString() {
        StringBuilder content = new StringBuilder(String.format(
                "%s, %d ects, studiejaar %d",
                naam,
                ects,
                studiejaar
        ));

        return content.toString();
    }
}
