package com.company;

/**
 * BsaMonitor3 - 12/11/2018
 *
 * @author Mehmet TETIK
 * <p>
 * Purpose:
 * <p>
 * Nieuwe vakken aanmaken en cijfers kunnen invoeren om te berekenen
 * hoeveel studiepunten men heeft gekregen aan de hand van de cijfer die hij/zij invoert
 */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Maximum aantal vakken die er zijn
        final int AANTAL_VAKKEN = 7;
        // Maximum aantal studiepunten die te behalen zijn
        final int MAX_AANTAL_STUDIEPUNTEN = 28;
        // Totaal aantal studiepunten dat behaald is door student
        int totaalAantalPuntenBehaald = 0;

        // Maak nieuwe array 'Vakken' met maximum lengte van maximum aantal vakken
        Vak[] vakken = new Vak[AANTAL_VAKKEN];

        Scanner scanner = new Scanner(System.in);

        // Vul array in met alle vakken die er zijn
        vakken[0] = new Vak("Fasten Your Seatbelts", 12);
        vakken[1] = new Vak("Programming", 3);
        vakken[2] = new Vak("User Interaction", 3);
        vakken[3] = new Vak("Personal Skills", 2);
        vakken[4] = new Vak("Databases", 3);
        vakken[5] = new Vak("OOP 1", 3);
        vakken[6] = new Vak("Project Skills", 2);

        System.out.println("Voer behaalde cijfers in:");

        // voor alle vakken, druk af en laat gebruiker zijn cijfers invoeren
        for (int i = 0; i < vakken.length; i++) {
            System.out.print(vakken[i].getNaam() + ": ");
            vakken[i].setCijfer(scanner.nextDouble());

            // Als vak met een voldoende is afgerond, tel studiepunten bij elkaar op
            if (vakken[i].getCijfer() >= 5.5) {
                totaalAantalPuntenBehaald += vakken[i].gehaaldePunten();
            }
        }

        // Druk alle vakken af met bijbehorende cijfer en studiepunten die behaald zijn per vak
        for (int i = 0; i < vakken.length; i++) {
            System.out.printf(
                    "%nVak/Project: %-22s Cijfer: %.1f Punten: %d",
                    vakken[i].getNaam(),
                    vakken[i].getCijfer(),
                    vakken[i].gehaaldePunten()
            );
        }

        // Druk totaal behaalde studiepunten af
        System.out.println("\n\nTotaal behaalde studiepunten: " + totaalAantalPuntenBehaald + "/" + MAX_AANTAL_STUDIEPUNTEN);

        // Geef melding weer
        System.out.println(geefMelding(totaalAantalPuntenBehaald, MAX_AANTAL_STUDIEPUNTEN));

    }

    /**
     * @param aantalBehaaldePunten      Aantal behaalde studiepunten door student
     * @param maxAantalBehaalbarePunten Max aantal studiepunten dat te behalen is
     * @return Geef waarschuwing weer mits student minder dan 83.33% studiepunten
     * heeft over het totaal aantal studiepunten dat te behalen is
     */
    public static String geefMelding(int aantalBehaaldePunten, int maxAantalBehaalbarePunten) {
        // Minimum percentage dat student moet hebben voor een positief BSA
        final double MIN_PERCENTAGE_VOOR_VOLDOENDE = 83.33;
        // Bereken percentage door behaalde studiepunten te delen door max behaalbare studiepunten
        int berekening = aantalBehaaldePunten / maxAantalBehaalbarePunten;
        // Declareer lege string
        String melding = "";

        // Als berekening onder de minimum percentage is, geef dan een waarschuwing weer
        if (berekening <= MIN_PERCENTAGE_VOOR_VOLDOENDE) {
            melding = "PAS OP: je ligt op schema voor een negatief BSA!";
        }

        return melding;
    }
}
