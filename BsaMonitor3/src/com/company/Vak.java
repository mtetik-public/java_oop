package com.company;

/**
 * BsaMonitor3 - 20/11/2018
 *
 * @author Mehmet TETIK
 *
 * Purpose:
 *
 * Alle getters/setters die bij het class 'Vak' horen
 */

public class Vak {

    private String naam;
    private int punten;
    private double cijfer;

    /**
     * @param nieuweNaam   Naam voor het vak
     * @param nieuwePunten Punten die bij het vak bijhoren
     */
    Vak(String nieuweNaam, int nieuwePunten) {
        this.naam = nieuweNaam;
        this.punten = nieuwePunten;
    }

    /**
     * @return Naam van het vak verkrijgen
     */
    public String getNaam() {
        return this.naam;
    }

    /**
     * @return Punten verkrijgen die bij het vak horen
     */
    public int getPunten() {
        return this.punten;
    }

    /**
     * @return Verkrijg alle cijfers die ingevuld zijn door de gebruiker
     */
    public double getCijfer() {
        return this.cijfer;
    }

    /**
     * @param nieuwCijfer Voer cijfer in dat bij het vak hoort
     */
    public void setCijfer(double nieuwCijfer) {
        this.cijfer = nieuwCijfer;
    }

    /**
     * @return Aantal gehaalde punten voor een vak dat is afgerond met een voldoende
     */
    public int gehaaldePunten() {
        if(cijfer >= 5.5){
            punten = punten;
        }else{
            punten = 0;
        }

        return punten;
    }
}
