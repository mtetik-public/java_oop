/**
 * oefenopdracht-3a - 09/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Cijfer {

    private int cijfer;

    public Cijfer() {
        this.cijfer = 0;
    }

    public int getCijfer() {
        return cijfer;
    }

    public void setCijfer(int cijfer) {
        this.cijfer = cijfer;
    }

    public int volgende() {
        if(cijfer == 9) {
            cijfer = 0;
            return cijfer;
        }
        return cijfer++;
    }
}
