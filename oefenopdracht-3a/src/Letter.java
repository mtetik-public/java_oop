/**
 * oefenopdracht-3a - 09/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Letter {

    private char letter;

    public Letter() {
        this.letter = 'A';
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public char volgende() {
        if(letter == 'Z') {
            letter = 'A';
            return letter;
        }
        return letter++;
    }
}
