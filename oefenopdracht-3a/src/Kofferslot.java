/**
 * oefenopdracht-3a - 09/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Kofferslot {

    public final static int AANTAL_KARAKTERS = 2;

    private Letter[] letters = new Letter[AANTAL_KARAKTERS];
    private Cijfer cijfer;

    public Kofferslot() {
        letters[0] = new Letter();
        letters[1] = new Letter();
        cijfer = new Cijfer();
    }

    public Kofferslot(char firstLetter, char secondLetter, int cijfer) {
        this();
        this.letters[0].setLetter(firstLetter);
        this.letters[1].setLetter(secondLetter);
        this.cijfer.setCijfer(cijfer);
    }

    public String volgende() {
        this.cijfer.volgende();

        if (this.letters[1].getLetter() == 'Z' && this.cijfer.getCijfer() == 0) {
            this.letters[0].volgende();
        }

        if (this.cijfer.getCijfer() == 0) {
            this.letters[1].volgende();
        }

        StringBuilder string = new StringBuilder();

        string.append(this.letters[0].getLetter());
        string.append(this.letters[1].getLetter());
        string.append(this.cijfer.getCijfer());

        return string.toString();
    }


}
