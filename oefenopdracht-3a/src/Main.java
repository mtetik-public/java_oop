/**
 * oefenopdracht-3a - 09/01/2019
 *
 * @author Mehmet TETIK
 * Purpose:
 */
public class Main {
    public static void main(String[] args) {
        Letter koffer = new Letter();
        Cijfer cijfer = new Cijfer();
        Kofferslot kofferslot = new Kofferslot('D', 'Z', 9);

        for (int i = 0; i < 30; i++) {
            System.out.println(koffer.getLetter());
            koffer.volgende();
        }

        for (int i = 0; i < 30; i++) {
            System.out.println(cijfer.getCijfer());
            cijfer.volgende();
        }

        System.out.println(kofferslot.volgende());

    }
}
