import java.time.LocalDate;
import java.util.Scanner;

/**
 * practicum3-student - 26/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Een klas kunnen aanmaken en daarin studenten kunnen aanmaken en toevoegen.
 * Alle studenten van een klas kunnen printen
 */
public class Main {
    public static void main(String[] args) {
        int studentnummer;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Voeg klas toe: ");
        String klasNaam = scanner.next();

        Klas klas = new Klas(klasNaam);

        // Klas.MAX_AANTAL_STUDENTEN

        for (int i = 0; i < Klas.MAX_AANTAL_STUDENTEN; i++) {
            do {
                System.out.println("Voer studentnummer in: ");
                studentnummer = scanner.nextInt();

                System.out.println("Voer voornaam in: ");
                String voornaam = scanner.next();

                System.out.println("Voer achternaam in: ");
                String achternaam = scanner.next();

                System.out.println("Voer geboortedatum in: (dd/mm/yyyy) ");
                String geboortedatum = scanner.next();

                String[] datum = geboortedatum.split("/");
                int dag = Integer.parseInt(datum[0]);
                int maand = Integer.parseInt(datum[1]);
                int jaar = Integer.parseInt(datum[2]);

                System.out.println("Voer straatnaam in: ");
                String straatnaam = scanner.next();

                System.out.println("Voer huisnummer in: ");
                int huisnr = scanner.nextInt();

                System.out.println("Voer postcode in: ");
                String postcode = scanner.next();

                System.out.println("Voer woonplaats in: ");
                String woonplaats = scanner.next();

                klas.voegStudentToe(
                        new Student(
                                studentnummer,
                                voornaam,
                                achternaam,
                                LocalDate.of(jaar, maand, dag),
                                new Adres(
                                        straatnaam,
                                        huisnr,
                                        postcode,
                                        woonplaats
                                )
                        )
                );
            }while(studentnummer != 0);

            System.out.println(klas.toString());
        }

    }
}
