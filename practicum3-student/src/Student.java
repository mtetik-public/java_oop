import java.time.LocalDate;

/**
 * practicum3-student - 26/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Student gegevens kunnen opslaan:
 *      - student kunnen aanmaken
 *      - geboortedatum student kunnen ophalen
 *      - student gegevens ophalen
 */
public class Student {
    private int studentnr;
    private String voornaam;
    private String achternaam;
    private LocalDate geboortedatum;
    private Adres adres;

    /**
     *
     * @param studentnr studentnummer student
     * @param voornaam voornaam student
     * @param achternaam achternaam student
     * @param geboortedatum geboortedatum student
     * @param adres adres gegevens student
     */
    public Student(int studentnr, String voornaam, String achternaam, LocalDate geboortedatum, Adres adres){
        this.studentnr = studentnr;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.geboortedatum = geboortedatum;
        this.adres = adres;
    }

    /**
     *
     * @return geboortedatum van student
     */
    private String korteGeboortedatum(){
        int dag = geboortedatum.getDayOfMonth();
        int maand = geboortedatum.getMonthValue();
        int jaar = geboortedatum.getYear();

        return dag + "-" + maand + "-" + jaar;
    }

    /**
     *
     * @return student gegevens
     */
    public String toString(){
        return studentnr + " " + voornaam + " " + achternaam +
                " (" + korteGeboortedatum() + ")\nAdres: " + adres.toString();
    }
}
