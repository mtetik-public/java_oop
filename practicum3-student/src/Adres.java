/**
 * practicum3-student - 26/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Adres gegevens van student kunnen opslaan:
 *      - adres student kunnen toevoegen
 *      - adres gegevens student kunnen ophalen
 *      - adres postcode kunnen controleren/checken
 */
public class Adres {
    private String straat;
    private int huisnr;
    private String postcode;
    private String plaats;

    /**
     *
     * @param straat straatnaam student
     * @param huisnr huisnummer adres student
     * @param postcode postcode adres student
     * @param plaats woonplaats student
     */
    public Adres(String straat, int huisnr, String postcode, String plaats){
        this.straat = straat;
        this.huisnr = huisnr;
        this.postcode = postcode;
        this.plaats = plaats;
    }

    /**
     *
     * @return haal adres gegevens op van student
     */
    public String toString(){
        // return straat + " " + huisnr + ", " + postcode + " " + plaats;
        StringBuilder resultaat = new StringBuilder();
        resultaat.append(straat);
        resultaat.append(" ");
        resultaat.append(huisnr);
        resultaat.append(", ");
        resultaat.append(postcode);
        resultaat.append(" ");
        resultaat.append(plaats);

        return resultaat.toString();
    }

    /**
     *
     * @param postcode postcode student
     * @return true als postcode daadwerkelijk een postcode is
     */
    public static boolean checkPostcode(String postcode){
        boolean correctPostcode = false;

        if(postcode.length() == 6) {
            if (postcode.matches("[1-9][0-9][0-9][0-9][a-zA-Z][a-zA-Z]+")) {
                correctPostcode = true;
            }
        }

        return correctPostcode;
    }

}
