/**
 * practicum3-student - 26/11/2018
 *
 * @author Mehmet TETIK
 * Purpose:
 *
 * Informatie over een klas:
 *      - klas kunnen aanmaken
 *      - aantal studenten van een klas kunnen halen
 *      - studenten toevoegen aan een klas
 *      - alle studenten kunnen weergeven die bij een klas horen
 */
public class Klas {
    public static final int MAX_AANTAL_STUDENTEN = 30;

    private String naam;
    private int aantalStudenten;
    private Student[] studenten;

    /**
     *
     * @param naam klas naam
     */
    public Klas(String naam) {
        aantalStudenten = 0;
        this.naam = naam;
        studenten = new Student[MAX_AANTAL_STUDENTEN];
    }

    /**
     *
     * @return aantal studenten die in een klas zitten
     */
    public int getAantalStudenten() {
        return this.aantalStudenten;
    }

    /**
     *
     * @param student student object toevoegen aan een specifieke klas
     * @return true als het is gelukt, anders false
     */
    public boolean voegStudentToe(Student student) {
        if(aantalStudenten == MAX_AANTAL_STUDENTEN){
            return false;
        }

        studenten[aantalStudenten++] = student;

        return true;
    }

    /**
     *
     * @return alle studenten die bij een klas horen
     */
    public String toString() {
        StringBuilder content = new StringBuilder(String.format(
                "Klas %s (%d studenten)\n",
                this.naam,
                getAantalStudenten()
        ));

        for (int i = 0; i < this.aantalStudenten; i++) {
            content.append(String.format("%s\n", studenten[i].toString()));
        }

        return content.toString();
    }
}
